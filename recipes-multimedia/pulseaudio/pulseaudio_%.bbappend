# Use 24bits 48kHz on the libretech-cc
do_compile_append_aml-s905x-cc () {
	set_cfg_value src/daemon/daemon.conf default-sample-rate 48000
	set_cfg_value src/daemon/daemon.conf alternate-sample-rate 44100
	set_cfg_value src/daemon/daemon.conf default-sample-format s24le
}
